#!/usr/bin/env python

import os
import argparse
import configparser
import paramiko

from orm.engine import Engine

db = Engine()
config = configparser.ConfigParser()
config.read("settings.ini")

host = config['server']['host']
port = config['server'].getint('port')
login = config['user']['login']
password = config['user']['password']


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('command')
    parser.add_argument('--dir')
    parser.add_argument('--path')
    parser.add_argument('--name')
    return parser


client = paramiko.SSHClient()
client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
client.connect(hostname=host, port=port, username=login, password=password)
sftp = client.open_sftp()

parser = createParser()
namespace = parser.parse_args()


if namespace.command == 'ls':
    directory = namespace.dir

    if not directory:
        raise Exception('The following arguments are required: --dir')

    stdin, stdout, stderr = client.exec_command('ls -l ' + directory)
    for line in stdout.readlines():
        print(line)

if namespace.command == 'download':
    directory = namespace.dir
    name = namespace.name

    if not name:
        raise Exception('The following arguments are required: --name')
    if not directory:
        directory = '/out'

    sftp.get(os.path.join(directory, name), name)
    db.add_download(name)


if namespace.command == 'upload':
    directory = namespace.dir
    path = namespace.path

    if not path:
        raise Exception('The following arguments are required: --path')
    if not directory:
        directory = '/in'

    name = path.split('/')[-1]
    directory = os.path.join(directory, name)
    sftp.put(path, directory)
    db.add_upload(name)

sftp.close()
client.close()


