import json
import configparser

config = configparser.ConfigParser()
config.read("settings.ini")
path = config['db']['path']


class Engine:

    def _read_db(self):
        with open(path, "r") as read_file:
            return json.load(read_file)

    def _save_db(self, data):
        with open(path, "w") as write_file:
            json.dump(data, write_file, indent=4)

    def _add_info(self, direction, name):
        data = self._read_db()
        if not direction in data:
            data[direction] = {
                    1: {"id": 1, "file_name": name},
                }
        else:
            last_index = max(int(elem) for elem in data[direction].keys())
            index = last_index + 1
            data[direction][index] = {"id": index, "file_name": name}

        self._save_db(data)

    def add_download(self, name):
        self._add_info("downloads", name)

    def add_upload(self, name):
        self._add_info("uploads", name)

